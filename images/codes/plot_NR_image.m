clear
close all
clc

LineWidth = 2;
Border = 2;
FontSize = 16;
%% Plot image
x = 0:0.1:20;
y = 0:0.1:20;
A = dlmread('../data/NR771_80nM_QD655_water_775nm_1uW_circular_745SP.dat',',',4,0); % From 2017-09-20.
imagesc(x-10,y-6,A);
c = colorbar;
c.Label.String = 'counts/ms';
colormap hot
xlabel('x (\mum)');
ylabel('y (\mum)');
axis image
xlim([0 10]);
ylim([0 10]);
set(gca,'Fontname','SansSerif')
set(gca,'FontSize',FontSize)
saveas(gcf, '..\NR_PL_image', 'svg')
saveas(gcf, '..\NR_PL_image', 'png')