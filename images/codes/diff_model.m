function y = diff_model( beta,x )
%Diffusion model of a molecule in a confocal microscope.
%   Detailed explanation goes here
y = beta(1)./((1+(x./beta(2))).*(1+2.7654^(-2).*(x./beta(2))).^(1/2));

end

