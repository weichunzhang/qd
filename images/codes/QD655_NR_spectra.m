% test spectra load

close all
clear
clc

addpath('Z:\Weichun\Matlab_programs\Spectra\')
folder = '..\data\'


LineWidth= 2;
Border = 2;
FontSize = 15;

%% SINGLE NR data
%load bg
% [wb sb]=spectra_load(strcat(folder,'4NR1Aug15_in_6mM_HCl_green_excitation_NR0_1.dat'));     % load bkg
% load spectra
% [w s] = spectra_load(strcat(folder,'4NR1Aug15_in_6mM_HCl_green_excitation_NR38_1.dat'));    % load NR spectra
% S = smooth((s-sb),10)./max(smooth((s-sb),10));
% clear s sb wb
%% bluk NR data
a = csvread(strcat(folder,'Nanopartz_A12-40-780-CTAB-replacement.csv'),2,0);
w = a(:,1);
S = a(:,2)/max(a(find(a(:,1)>400),2));

clear a
plot(w,S,'k','LineWidth',LineWidth);
% h(1).FaceColor = [218,165,32]/255;
% title('Single Rod Luminiscence')
hold all
% end
% h(1).FaceAlpha = 0.5;
clear w s


%%
% absorption
data = csvread(strcat(folder,'Qdot_655_spectrum.csv'),1,0);
% wa=data(:,1);
% a=data(:,2);
% emission

we=data(:,1);
e=data(:,3);
clear data
%%
figure(1)

plot(we,e./max(e),'color',[0.9 0.0 0.3],'LineStyle','-','LineWidth',LineWidth);
hold all
% plot(wa,a./max(a),'color',[0 0.5 1],'LineWidth',LineWidth);
plot([775 775],[-0.1 1.1],':k','LineWidth',LineWidth) % laser line

% labels and axis
% set(gcf,'units','centimeters')
% set(gcf,'position',[8 8 16 9])
% % set(gcf,'position',[482   214   956   815])
ylim([0 1.05])
xlim([400 900])

l=legend('Ensemble AuNR','QD emission','775 nm Laser');
legend('boxoff');
set(l,'location','northwest')
set(gca,'Fontname','Arial')
set(gca,'LineWidth',Border)
set(gca,'FontSize',FontSize)
xlabel('Wavelength (nm)','FontSize',FontSize)
ylabel('Normalized intenstiy ','FontSize',FontSize)

saveas(gcf, '..\QD_and_NR_spectra', 'svg')
