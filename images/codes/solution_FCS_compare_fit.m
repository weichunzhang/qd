clc
close all
clear

% This code plots the time trace recorded from 16 nM QD.
% This code compares the autocorrelations from the period without high
% bursts and the entire recorded time traces. It also fits the
% autocorrelation without burst with the diffusion model.
%% plot settings
LineWidth = 2;
Border = 2;
FontSize = 16;

A = dlmread('..\data\solution_20uW_FCS_whole_trace.dat','',1,0);
ind1 = find(A(:,1)>= 1e-2);
tau1 = A(ind1,1);
AC1 = A(ind1,2);
B = dlmread('..\data\solution_20uW_FCS_without_bursts.dat','',1,0);
ind2 = find(B(:,1)>= 1e-2);
tau2 = B(ind2,1);
AC2 = B(ind2,2);

C = dlmread('..\data\Solution_20uW_time_trace_10ms.dat','',1,0);
subplot(2,1,1)
plot(C(:,1),C(:,2)*1000,'k','LineWidth',LineWidth-1);
xlabel('Time (s)');
ylabel('photons/sec');
set(gcf,'units','centimeters')
set(gcf,'position',[5 5 16 12]); % set(gcf,'position',[left bottom width height]) 
set(gca,'Fontname','SansSerif')
set(gca,'FontSize',FontSize)
set(gca,'linewidth',Border,'FontSize',FontSize)
% Fit the second correlation curve
beta0 = [1 1]; % initial parameter values
[para,R,J,CovB] = nlinfit(tau2,AC2,'diff_model',beta0);
AC_fit = diff_model(para,tau2);

subplot(2,1,2)
semilogx(tau1,AC1,'k^',tau2,AC2,'rv',tau2,AC_fit,'r','LineWidth',LineWidth);
xlabel('Lag time \tau (ms)');
ylabel('{\it{g}^{(2)}(\tau) - 1}');
set(gca, 'XTick', [1e-2 1e-1 1 1e1 1e2 1e3]);
legend('Whole trace','Selected trace','Diffusion model');
xlim([1e-2 1e3]);
ylim([-0.02 1.1]);

% Fitting uncertainty
paraci = nlparci(para,R,'jacobian',J,'alpha',0.05);
err_G0 = paraci(1,2)-para(1)
err_tau_D = paraci(2,2)-para(2)

text(10,0.5,strcat('{\it{g}^{(2)}(0) = }',num2str(para(1),2)),'FontSize',FontSize); % with 2 significant digits.
text(10,0.3,strcat('{\tau_D = }',num2str(para(2),2),'{ ms}'),'FontSize',FontSize); % with 2 significant digits.

set(gcf,'units','centimeters')
set(gcf,'position',[5 5 16 22]); % set(gcf,'position',[left bottom width height]) 
set(gca,'Fontname','SansSerif')
set(gca,'FontSize',FontSize)
set(gca,'linewidth',Border,'FontSize',FontSize)
saveas(gcf, '..\QD_normal_FCS', 'svg')
saveas(gcf, '..\QD_normal_FCS', 'eps')