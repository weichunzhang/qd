close all
clear
clc
%% plot settings
LineWidth = 2;
Border = 2;
FontSize = 16;

%% Read data
folder = '..\data\'

bg = csvread(strcat(folder,'Power_dependence_Sample=NR771_16nM_QD_1mM_NaCl_water_775nm_circular_solution_lambda=775nm_Meas_at_18hs47m_TimeTrace_Power=0uW.dat'),2,0);

bkg_mean = mean(bg)*100;

file = dir(strcat(folder,'Power_dependence_Sample=NR771_16nM_QD_1mM_NaCl_water_775nm_circular_solution_lambda=775nm_Meas_at_18hs47m_averaged.dat'));

a = csvread(strcat(folder,file.name),1,0);
pw = a(5:end,1); % power at bfp
dt = 1; % sec

c2 = a(5:end,3)/dt; % counter 2


ch2 = c2 - bkg_mean;
% e =sqrt(e1.^2+e2.^2+bkg_std.^2); % propagation of uncertainty.

%% fit
p_poly=polyfit(log(pw),log(ch2),1);
%%
figure(1)
plot(pw,ch2,'sb');
hold all
loglog(pw,exp(polyval(p_poly,log(pw))),'r--')

set(gca,'yscale','log')
set(gca,'xscale','log')
% ,strcat('fit. Slope=',num2str(p2_EtOH(1)))
l=legend('Data',strcat('fit. Slope=',num2str(p_poly(1))));
set(l,'location','northwest')
xlabel('Power at sample (\muW)')
ylabel('Mean Detected counts (cps)')
title('16 nM QD655 in NaCl excited with 775 nm')
grid on

%%  In order to get rid of the burst and calculate more accurately the mean counts, I fit the traces with Poissonian. Analyze the background time trace separately
figure(2)
histfit(bg',[],'poisson')
pd_bg = fitdist(bg','poisson');
xlabel('photons/10ms');
ylabel('Occurence');
title('Dark counts');
bg_center = pd_bg.lambda*100; % Convert to counts/s.
bg_ci = paramci(pd_bg)*100; % Confidence interval with alpha = 0.05.
bg_err = bg_ci(2)-bg_center;

%% In order to get rid of the burst and calculate more accurately the mean counts, I fit the traces with Poissonian.
% Now the time traces with laser on.
files = dir(strcat(folder,'Power_dependence_Sample=NR771_16nM_QD_1mM_NaCl_water_775nm_circular_solution_lambda=775nm_Meas_at_18hs47m_TimeTrace_Power=*.*.dat'));
% Read only the time traces with laser on.

for N = 1:size(files,1)
    data.a = csvread(strcat(folder,files(N).name),2,0);
    aux1 = strfind(files(N).name,'='); % Find the power number
    aux2 = strfind(files(N).name,'u'); % Find the power number
    data.power(N) = str2double(files(N).name(aux1(end)+1:aux2(end)-1))/90;
    h = figure(N+2);
    set(h,'visible','on');
    histfit(data.a',[],'poisson')
    data.pd(N) = fitdist(data.a','poisson');
    xlabel('photons/10ms');
    ylabel('Occurence');
    title(strcat(num2str(data.power(N)),'\muW'));
    
    data.cnt_center(N) = data.pd(N).lambda*100 - bg_center;
    cnt_ci = paramci(data.pd(N))*100;
    data.cnt_var(N) = cnt_ci(2) - data.pd(N).lambda*100;% Without error propagation.
    data.cnt_err(N) = sqrt(bg_err^2+data.cnt_var(N)^2); % With error propagation.
    
    clear aux1 aux2 data.a cnt_ci
end

%% Plot the power dependence curve with errorbars.
figure(N+3)
power_min = 4; % The power limit for fitting
errorbar(data.power(find(data.power>power_min)),data.cnt_center(find(data.power>power_min)),data.cnt_err(find(data.power>power_min)),'s','LineWidth',LineWidth)
set(gca,'yscale','log')
set(gca,'xscale','log')
hold on
% Only the last 8 powers generate possitive net count.

%% Now fit for the slope, taking the errorbar into acount.
log_power = log(data.power(find(data.power>power_min)));
log_cnt = log(data.cnt_center(find(data.power>power_min)));
log_err = data.cnt_err(find(data.power>power_min))./data.cnt_center(find(data.power>power_min)); % https://terpconnect.umd.edu/~toh/models/ErrorPropagation.pdf
power_err = zeros(1,length(log_power)); % Power has no uncertainty.

%% fit 
% % Fit with polyfit
% % https://www.mathworks.com/matlabcentral/fileexchange/39126-polyparci
% [p_poly,S]= polyfit(log_power,log_cnt,1);
% loglog(exp(log_power),exp(polyval(p_poly,log_power)),'r');
% ci_poly = polyparci(p_poly,S);
% slope_poly = p_poly(1); % Slope of the fitting
% slope_poly_uncert = (ci_poly(2,1)-ci_poly(1,1))/2;
% 
% disp(strcat('Polyfit,slope = ',num2str(slope_poly),' +/- ',num2str(slope_poly_uncert)))

% Fit with curve fitting toolbox
% https://stats.stackexchange.com/questions/56596/finding-uncertainty-in-coefficients-from-polyfit-in-matlab
cf = fit(log_power',log_cnt','poly1');
cf_coeff = coeffvalues(cf);
cf_confint = confint(cf);
slope_fit = cf_coeff(1);
slope_fit_uncert = (cf_confint(2,1) - cf_confint(1,1))/2;
loglog(exp(log_power),exp(cf_coeff(1)*log_power+cf_coeff(2)),'r','LineWidth',LineWidth);
disp(strcat('Fit, slope = ',num2str(slope_fit),' +/- ',num2str(slope_fit_uncert)))

% Conclusion: the coefficients are the same from both methods, but the
% uncertainties for coefficients from 'fit' is larger than polyfit. I
% choose to use fit.

% %% Fit also the errorbars in the data
% % The following method is from
% % https://www.mathworks.com/matlabcentral/fileexchange/17466-weighted-total-least-squares-straight-line-fit
% [a_wtls,b_wtls,alpha,p_wtls,chiopt,Cab,Calphap]=...
%    wtls_line(log_power,log_cnt,power_err,log_err); %  weighted total least squares straight line fit y=a*x+b
% loglog(exp(log_power),exp(a_wtls*log_power+b_wtls),'r');
% slope_wtls_uncert = Cab(1);
% disp(strcat('WTLS fit, slope = ',num2str(a_wtls),' +/- ',num2str(slope_wtls_uncert)))
% 
% % The following method is from 
% % https://www.mathworks.com/matlabcentral/fileexchange/26586-linear-regression-with-errors-in-x-and-y
% [york_a, york_b, sigma_a, sigma_b] =...
%    york_fit(log_power,log_cnt,1e-9,log_err); %estimate parameters % (Y=a+b*X)
% % Use a very small number for the error of X, because it cannot be zero.
% loglog(exp(log_power),exp(york_b*log_power+york_a),'m');
% slope_york_uncert = sigma_b;
% disp(strcat('York fit, slope = ',num2str(york_b),' +/- ',num2str(slope_york_uncert)))
% % If I fit the errorbars, the uncertainties for the slope are actually smaller!

%% The main plot
legend('Data',strcat('{Fit. Slope = }',num2str(2.11),'{ \pm }',num2str(0.03)));
legend('location','northwest')
xlabel('Power at the sample (\muW)');
ylabel('Photons/sec');
xlim([4 50])
grid on
hold off
set(gcf,'units','centimeters')
set(gcf,'position',[5 5 16 12]); % set(gcf,'position',[left bottom width height]) 
set(gca,'Fontname','SansSerif')
set(gca,'FontSize',FontSize)
set(gca,'linewidth',Border,'FontSize',FontSize)

saveas(gcf, '..\QD_two_photon_power_dependence', 'png')